// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyHTML = require('gulp-minify-html');
var gulpFilter = require('gulp-filter');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('dist/css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('src/js/*.js')
        .pipe(concat('buy-dialog.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(rename('buy-dialog.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

// Move templates & serve-dialog.php
gulp.task('templates', function () {
    var htmlFilter = gulpFilter('**/*.html', {restore: true});

    return gulp.src(['src/html/*.html', 'src/*.php', 'src/*.json'])
        .pipe(htmlFilter)
        .pipe(minifyHTML({ quotes: true }))
        .pipe(htmlFilter.restore)
        .pipe(gulp.dest('dist'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(['src/html/*.html', 'src/*.json'], ['templates']);
    gulp.watch('src/js/*.js', ['lint', 'scripts']);
    gulp.watch('src/scss/*.scss', ['sass']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'templates', 'watch']);