(function ($, w) {
    /**
     * For Development purposes we're injecting this (local) script into an existing website.
     * Due to this setting we'll need to load our external resources (js, css) programmatically.
     *
     * On the production environment we'll include these in <link> and <script> tags accordingly.
     */
    var scriptLoader = $.Deferred(),
        initializer = $.Deferred(),
        pluginsLoaded = $.Deferred();

    // Bootstrap scripts
    var _popupOverlay = $.getScript('https://cdn.rawgit.com/vast-engineering/jquery-popup-overlay/1.7.10/jquery.popupoverlay.js', function () {
        scriptLoader.resolve();
    });

    var _ajaxChimp = $.getScript('https://cdn.rawgit.com/scdoshi/jquery-ajaxchimp/master/jquery.ajaxchimp.js');
    var _cookies = $.getScript('https://cdn.rawgit.com/js-cookie/js-cookie/master/src/js.cookie.js');

    $.when(_ajaxChimp, _cookies).done(function () {
        pluginsLoaded.resolve();

        // Set default domain
        Cookies.defaults.domain = location.hostname.split(".").slice(-2).join(".");
    });


    // Inject CSS
    $('<link>')
        .appendTo('head')
        .attr({type: 'text/css', rel: 'stylesheet'})
        .attr('href', 'http://dev.smartspirometry.com/css/styles.css');

    /**
     * End of Script & Style Injection
     */

    /**
     * The Dialog object, responsible for the wizard and its visibility.
     * @param {object} options
     * @constructor
     */
    var Dialog = function (options) {
        var self = this;

        self.element = null;
        self.stage = null;
        self.config = $.extend({
            shopUrl: 'https://shop.smartspirometry.com',
            homepageUrl: 'http://smartspirometry.com',
            dialog: null,
            forceSelection: false
        }, options);

        /**
         * Initializes the dialog on the given element.
         * @param {string} dialogReference
         * @returns {object}
         */
        self.initialize = function () {
            // Load the countries
            var loadCountries = self.loadCountries();

            // Load the Dialog's content and proceed
            var loadDialogContent = self.loadDialogContent();

            $.when(loadCountries, loadDialogContent, pluginsLoaded).done(function () {
                // Initialize Popup
                self.initializePopup();

                // Populate Countries
                self.populateCountries();

                // Automatically open in case of forced available country?
                if (self.showDialogOnLanding()) {
                    self.show();
                }
            });

            return self.element;
        };

        /**
         * Initializes the Popup
         */
        self.initializePopup = function () {
            var options = {
                transition: 'all 0.3s',
                scrolllock: true,
                closeelement: '.closer'
            };

            if (self.forced()) {
                options = $.extend(options, {
                    closeelement: null,
                    blur: false,
                    escape: false
                });

                // Hide the close button
                self.element.find('.closer').hide();
            }

            self.element.popup(options);
        }

        /**
         * Loads the HTML for the Dialog content either from a URL or an existing element.
         * @returns {*}
         */
        self.loadDialogContent = function () {
            var deferred = $.Deferred();

            // Loading template through HTML?
            if (self.config.dialog.indexOf('http') > -1) {
                self.element = $('<div id="countryBuyDialog">').appendTo('body');
                self.element.load(self.config.dialog, function () {
                    deferred.resolve();
                });
            } else {
                self.element = $(self.config.dialog);

                deferred.resolve();
            }

            // When loaded
            deferred.then(function () {
                // Bind stage
                self.stage = self.element.find('#stage');

                initializer.resolve();
            });

            return deferred;
        }

        /**
         * Shows the dialog.
         */
        self.show = function () {
            initializer.then(function () {
                self.element.popup('show');

                self.transition('welcome');
            });
        };

        /**
         * Closes the dialog.
         */
        self.close = function () {
            self.element.popup('hide');
        };

        /**
         * Transitions to a certain step within the flow.
         * @param {string} toStep
         * @param {string} data
         */
        self.transition = function (toStep, data) {
            switch (toStep) {
                case 'eu-country-selected':
                    return self.handleCountrySelection(data);
                case 'link-to-shop':
                    self.forwardToShop(false, data);
                    break;
                case 'link-to-homepage':
                    self.forwardToHomepage(false, data);
                    break;
            }

            // Get the step contents
            var content = self.getStepContent(toStep);

            // Fade out current, if any
            var fadedOut = self.stageFadeOut();

            // Replace content
            self.setStageContent(content, data);

            // Set stage class
            self.stage.attr('class', 'step-' + toStep);

            // Load Mailchimp
            self.loadMailchimp();

            // Fade in
            fadedOut.then(function () {
                self.stageFadeIn();
            });
        };

        /**
         * Handles branching to a certain step from within a step.
         * @param {object} element
         */
        self.triggerClickEvent = function (element) {
            self.transition(element.attr('data-step-to'), element.attr('data-step-data'));
        };

        /**
         * Populates the list of EU countries.
         */
        self.populateCountries = function () {
            initializer.then(function () {
                self.element.find('.eu-country-list').html('').each(function () {
                    for (var i in self.countries) {
                        $(this).append('<li><a href="#" data-step-to="eu-country-selected" data-step-data="' + i + '">' + self.countries[i].name + '</a></li>');
                    }
                });
            });
        };

        /**
         * Handles the selection of a country.
         * @param {string} countryName
         */
        self.handleCountrySelection = function (countryName) {
            if (self.countries[countryName]) {
                var country = self.countries[countryName];
                var params = {'COUNTRY': country.name};

                // Set country in cookie
                self.setCookie('country', country.name);
                self.setCookie('educational-intent', 'unknown');

                // If the country is available, forward user to shop
                if (country.available) {
                    return self.transition('link-to-shop', params);
                }

                self.transition('eu-country-unavailable', params);
            }
        };

        /**
         * Retrieves the HTML content for a given step.
         * @param {string} step
         * @returns {string}
         */
        self.getStepContent = function (step) {
            var step = self.element.find('[data-step="' + step + '"]');

            if (step.length == 0) {
                return 'STEP_NOT_FOUND';
            }

            return step.html();
        };

        /**
         * Sets a cookie for the webshop.
         * @param {string} key
         * @param {string|object} value
         */
        self.setCookie = function (key, value) {
            if (value === null) {
                Cookies.remove(key)
            } else {
                Cookies.set(key, value);
            }
        };

        /**
         * Fades out the stage, while retaining its height.
         */
        self.stageFadeOut = function () {
            var height = self.stage.innerHeight(),
                deferred = $.Deferred();

            self.stage.height(height);

            self.stage.stop().fadeOut(0, function () {
                self.stage.html('');

                deferred.resolve();
            }).html('');

            return deferred;
        };

        /**
         * Fades in the stage.
         */
        self.stageFadeIn = function () {
            self.stage.height('auto');
            self.stage.stop().hide().fadeIn(200);
        };

        /**
         * Sets the stage's content.
         * @param {string} content
         * @param {object} replaceables
         */
        self.setStageContent = function (content, replaceables) {
            // Modify content
            for (var r in replaceables) {
                content = content.replace(new RegExp('{{' + r + '}}', 'gi'), replaceables[r]);
            }

            self.stage.html(content);
        };

        /**
         * Forwards user to the shop.
         * @param {boolean} instant
         * @param {string} data
         */
        self.forwardToShop = function (instant, data) {
            // Mark user as having an educational intention?
            if (data === 'confirm-educational') {
                self.setCookie('country', null);
                self.setCookie('educational-intent', 'confirmed');
            }

            // Close popup if we're already in the shop
            if (self.inShop()) {
                return self.close();
            }

            self.forwardTo(self.config.shopUrl, instant);
        };

        /**
         * Forwards user to the homepage.
         * @param {boolean} instant
         * @param {string} data
         */
        self.forwardToHomepage = function (instant) {
            // Close popup if we're already on the homepage
            if (self.onHomepage()) {
                return self.close();
            }

            self.forwardTo(self.config.homepageUrl, instant);
        };

        /**
         * An instant or delayed forward.
         * @param {string} location
         * @param {boolean} instant
         */
        self.forwardTo = function (location, instant) {
            // Perform a redirection
            var perform = function () {
                w.location = location;
            };

            if (instant !== true) {
                setTimeout(perform, 500);
            } else {
                perform();
            }
        };

        /**
         * Initializes Mailchimp
         */
        self.loadMailchimp = function () {
            pluginsLoaded.then(function () {
                $.ajaxChimp.translations.en = {
                    'submit': 'Submitting...',
                    0: 'Thanks for subscribing! We\'ve sent you a confirmation email.',
                    1: 'Please enter an email address',
                    2: 'An email address must contain a single @',
                    3: 'The domain portion of the email address is invalid',
                    4: 'The username portion of the email address is invalid',
                    5: 'This email address looks fake or invalid. Please enter a real email address.',
                };

                $('form[name="email-signup"]').ajaxChimp({
                    language: 'en',
                    url: 'http://smartspirometry.us11.list-manage.com/subscribe/post-json?u=a5aec8f2487fd0607a7f3dde8&id=710cf3d380&c=',
                    callback: self.mailchimpCallback
                });

                self.stage.find('.hide-after-signup').show();
                self.stage.find('.show-after-signup').hide();
            });
        };

        /**
         * Handles a Mailchimp response.
         * @param {object} response
         */
        self.mailchimpCallback = function (response) {
            if (response.result === 'success') {
                self.stage.find('form[name="email-signup"] input').hide();
                self.stage.find('.hide-after-signup').hide();
                self.stage.find('.show-after-signup').hide().fadeIn(600);
            }
        };

        /**
         * Determines whether or not this dialog forces the user
         * to complete the steps and make their intention known.
         * @returns {boolean}
         */
        self.forced = function () {
            return self.config.forceSelection;
        };

        /**
         * Determines whether the visitor should be shown the dialog.
         * It looks if certain cookies are set and returns a result on this.
         */
        self.showDialogOnLanding = function () {
            // If the user is not forced to make a selection
            if (!self.forced()) {
                return false;
            }

            var country = Cookies.get('country'),
                intention = Cookies.get('educational-intent');

            // Check if an available country is set, in this case: false
            if (typeof(country) !== 'undefined') {
                var countryCode = null,
                    countryAvailable = false;

                // Get the country from the list
                for (var code in self.countries) {
                    if (self.countries[code].name === country) {
                        countryCode = code;
                        countryAvailable = self.countries[code].available;
                    }
                }

                // No valid or available country was set
                if (!countryAvailable) {
                    // A country was found in the cookie we don't recognize
                    if (!countryCode) {
                        self.setCookie('country', null);

                        return true;
                    }

                    // @todo: Show Dialog or Redirect?

                    return true;
                }

                return false;
            }

            // No country is set, check educational intention
            if (typeof(intention) !== 'undefined') {
                switch (intention) {
                    case 'unknown':
                        // @todo: Show Dialog or Redirect?

                        return true;
                    case 'confirmed':
                        return false;
                }
            }

            // In any other case, show Dialog
            return true;
        };

        /**
         * Assigns the known countries based on the passed variable.
         * It can be either a URL to a JSON request or a passed object.
         */
        self.loadCountries = function () {
            var deferred = $.Deferred();

            if (self.config.countries) {
                if (typeof(self.config.countries) === 'string') {
                    $.getJSON(self.config.countries, function (countries) {
                        self.countries = countries;

                        // We'll repopulate the countries in case this happened before.
                        self.populateCountries();

                        deferred.resolve();
                    });
                } else {
                    self.countries = self.config.countries || [];

                    deferred.resolve();
                }
            }

            return deferred;
        };

        /**
         * Returns the domain for a config url.
         * @param url
         * @returns {string|void|XML}
         */
        self.getDomain = function (url) {
            return self.config[url].replace(/.*?:\/\//g, "");
        };

        /**
         * Determines if the user is currently in the shop.
         */
        self.inShop = function () {
            return location.hostname === self.getDomain('shopUrl');
        };

        /**
         * Determines if the user is currently on the homepage.
         */
        self.onHomepage = function () {
            return location.hostname === self.getDomain('homepageUrl');
        };

        // Initialize Dialog
        scriptLoader.then(function () {
            self.initialize();
        });
    };

    /**
     * Global helper to setup a dialog.
     * @param {object} options
     * @returns Dialog
     * @constructor
     */
    w.SetupCountryDialog = function (options) {
        var dialog = new Dialog(options);

        /**
         * Click Handlers
         */

        // On clicking a link to the Shop, only if we're not already in the Shop
        if (!dialog.inShop()) {
            $('body').on('click', 'a', function (e) {
                if ($(this).prop('href').indexOf(dialog.getDomain('shopUrl')) > -1) {
                    dialog.show();

                    return false;
                }
            });
        }

        // Branching from step to step
        $('body').on('click', '[data-step-to]', function (e) {
            dialog.triggerClickEvent($(this));

            return false;
        });

        return dialog;
    };

    $(document).ready(function () {
        return false;

        // Setup Dialog
        var dialog = SetupCountryDialog({
            shopUrl: 'http://shop.smartspirometry.com',
            homepageUrl: 'http://www.smartspirometry.com',
            dialog: 'http://dev.smartspirometry.com/serve.php?file=template',
            countries: 'http://dev.smartspirometry.com/serve.php?file=countries',
            forceSelection: true
        });

    });

})(jQuery, window);

/**
 * Usage Instructions
 * (using Development Defaults)
 *
 * These comment blocks are removed during the build-process.
 */

/**

 $(document).ready(function () {

    // Setup Dialog
    var dialog = SetupCountryDialog({
        shopDomain: 'shop.smartspirometry.com',
        dialog: 'http://dev.smartspirometry.com/serve.php?file=template',
        countries: 'http://dev.smartspirometry.com/serve.php?file=countries',
        forceSelection: false
    });

 });

 */