<?php

header('Access-Control-Allow-Origin: *');

if (empty($_GET['file'])) {
    return;
}

switch($_GET['file']) {
    case 'template':
        include ('dialog.tpl.html');
        break;
    case 'countries':
        header('Content-Type: application/json');

        include ('countries.json');

        break;
}

